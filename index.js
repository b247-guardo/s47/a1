const txtFirstName = document.getElementById('txt-first-name');
const txtLastName = document.getElementById('txt-last-name');
const spanFullName = document.getElementById('span-full-name');

txtFirstName.addEventListener('input', updateFullName);
txtLastName.addEventListener('input', updateFullName);

function updateFullName() {
	const firstName = txtFirstName.value;
	const lastName = txtLastName.value;
	const fullName = `${firstName} ${lastName}`;
	spanFullName.innerHTML = fullName;
};